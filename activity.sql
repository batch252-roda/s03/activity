mysql -u root -p

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE Users (
  user_id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  dateTime_created DATETIME NOT NULL,
  PRIMARY KEY(user_id)
);

INSERT INTO Users (email, password, dateTime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"),
("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"),
("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"),
("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"),
("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

CREATE TABLE Posts (
  user_id INT NOT NULL,
  title VARCHAR(25) NOT NULL,
  content VARCHAR(50) NOT NULL,
  dateTime_posted DATETIME NOT NULL,
  CONSTRAINT fk_users_user_id
  FOREIGN KEY (user_id) REFERENCES Users(user_id)
  ON UPDATE CASCADE
  ON DELETE RESTRICT
);

INSERT INTO Posts ( user_id, title, content, dateTime_posted )
VALUES 
(1, "First Code", "Hello World!", "2021-01-02 01:00:00"),
(1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"),
(2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"),
(5, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

SELECT * FROM Posts WHERE user_id = 1;

SELECT email, dateTime_created FROM Users;

UPDATE Posts SET content = "Hello to the people of the Earth" 
WHERE content = "Hello Earth!" AND user_id = 1;

DELETE FROM Users WHERE email = "johndoe@gmail.com";